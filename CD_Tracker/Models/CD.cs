﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CD_Tracker.Models
{
    public class CD
    {
        /* 
        1.restrict Rate from being set to anything > .99
        2.format Start & Finish to only show date
        3.format Deposit & Mature to display $???.00
        4.add validation to Bank
        5.make ListCds show up as index view
        6. make it look purdy
        7.make views utilize tag helpers
         */
        [Required(ErrorMessage = "Please select a your bank")]
        public string Bank { get; set; }

        [Required(ErrorMessage = "Please select the term length of your CD")]
        public int Term { get; set; }

        [Required(ErrorMessage = "Please provide your CDs intrest rate")]
        [Range(.01, .99,
               ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public double Rate { get; set; }

        [Required(ErrorMessage = "Please provide your CD data of puchase in the following format (mm/dd/yyyy)")]
        public DateTime Start { get; set; }

        public DateTime Finish { get; set; }

        [Required(ErrorMessage = "Please provide your CDs' starting value")]
        public double Deposit { get; set; }

        public double MatureValue { get; set; }

        public void Calculate()
        {
            // end date
            Finish = Start.AddMonths(Term);
            // mature Value
            /*
                
                A = P(1+r/n)(nt)
                A is the total that your CD will be worth at the end of the term, including the amount you put in.
                P is the principal, or the amount you deposited when you bought the CD.
                R is the rate, or annual interest rate, expressed as a decimal. If the interest rate is 1.25% APY, r is 0.0125.
                n is the number of times that interest in compounded every year.
                Most CDs pay interest that is compounded daily, so n = 365. Check with your bank to verify the interest is compounded daily.
                t is time, or the number of years until the maturity date.
                
              

             */

            if (Term <= 0)
            {
                throw new Exception("Term cannot be less than 1");
            }

            if (Rate < .01 || Rate > .99)
            {
                throw new Exception("Rate is out of acceptable range");
            }

            if (Deposit < .01)
            {
                throw new Exception("Deposit is too small");
            }
            int t = Term / 12;
            double r =  Rate;
            int n = 365;
            double a = (1 + r / n);
            double b = (t * n);
            double c = Math.Pow(a, b);
            MatureValue = Deposit * c;
        }


       
    }
}
