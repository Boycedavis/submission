﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_07.Models
{
    public class Director
    {
        public int DirectorID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public int Age { get; set; }
        public bool Active { get; set; }

        //Associations
        public List<MovieDirector> MovieDirectors { get; set; }
    }
}
