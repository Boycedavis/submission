﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_07.Models
{
   

        public interface IMovieRepository
        {
            IEnumerable<Movie> Movies { get; }

        }
    
}
