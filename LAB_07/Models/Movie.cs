﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_07.Models
{
    public class Movie
    {
        public int MovieID { get; set; }
        public string MovieName { get; set; }
        public string Genre { get; set; }
        public int Runtime { get; set; }
        public DateTime? Release { get; set; }

        //Relationships
        public int StudioID { get; set; }
        public Studio Studio { get; set; }

        public List<MovieDirector> MovieDirectors { get; set; }
    }
}
