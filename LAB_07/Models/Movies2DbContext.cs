﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_07.Models
{
    public class Movies2DbContext : DbContext
    {
        public Movies2DbContext(DbContextOptions<Movies2DbContext> options) : base(options)
        {

        }

        public DbSet<Studio> Studios { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Director> Directors { get; set; }
        public DbSet<MovieDirector> MovieDirectors { get; set; }
    }
}
