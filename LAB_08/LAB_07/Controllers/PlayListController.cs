﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAB_07.Models;
using LAB_07.Models.ViewModels;
using LAB_07.Infrastructure;


namespace LAB_07.Controllers
{
    public class PlayListController : Controller
    {
        private IMovieRepository movieRepository;

        public PlayListController(IMovieRepository movieRepository)
        {
            this.movieRepository = movieRepository;
                 
        }

        public RedirectToActionResult AddToPlayList(int movieId, string returnURL)
        {
            Movie movie = movieRepository.Movies.FirstOrDefault(m => m.MovieID == movieId);
            if(movie != null)
            {
                PlayList playList = GetPlayList();
                playList.AddMovie(movie);
                SavePlayList(playList);
            }
            return RedirectToAction("Index", new { returnURL });
        }

        public RedirectToActionResult RemoveFromPlayList(int movieId, string returnURL)
        {
            Movie movie = movieRepository.Movies.FirstOrDefault(m => m.MovieID == movieId);
            if (movie != null)
            {
                PlayList playList = GetPlayList();
                playList.RemoveMovie(movie);
                SavePlayList(playList);
            }
            return RedirectToAction("Index", new { returnURL });
        } 

        public IActionResult Index(string returnURL)
        {
            PlayListIndexViewmodel model = new PlayListIndexViewmodel()
            {
                PlayList = GetPlayList(),
                ReturnUrl = returnURL
            };
            return View(model);
        }
        private PlayList GetPlayList()
        {
            PlayList playList = HttpContext.Session.GetJson<PlayList>("PlayList") ?? new PlayList();
            return playList;
        }

        private void SavePlayList(PlayList playList)
        {
            HttpContext.Session.SetJson("PlayList", playList);
        }
        
    }
}
