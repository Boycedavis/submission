﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAB_07.Models;

namespace LAB_07.Controllers
{
    
    public class StudioController : Controller
    {
        private IStudioRepository studioRepository;
       

        public StudioController(IStudioRepository studioRepository)
        {
            this.studioRepository = studioRepository;
        }
        public IActionResult List() => View(studioRepository.Studios);
    }
}
