﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_07.Models
{
    public class EFMovieRepository : IMovieRepository
    {
        private Movies2DbContext context;

        public IEnumerable<Movie> Movies => context.Movies;

        public EFMovieRepository(Movies2DbContext context)
        {
            this.context = context;
        }

    }
}
