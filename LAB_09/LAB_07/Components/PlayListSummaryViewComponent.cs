﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAB_07.Models;
using Microsoft.AspNetCore.Mvc;

namespace LAB_07.Components
{
    public class PlayListSummaryViewComponent : ViewComponent
    {
        private PlayList playList;

        public PlayListSummaryViewComponent(PlayList playList)
        {
            this.playList = playList;
        }

        public IViewComponentResult Invoke()
        {
            return View(playList);
        }
    }
}
