﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_07.Models
{
    public interface IOrderRepository 
    {
      IEnumerable<Order> Orders { get; }

        void SaveOrder(Order order);
    }
}
