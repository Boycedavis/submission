﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAB_07.Models;
using LAB_07.Models.ViewModels;
namespace LAB_07.Controllers
{
    public class MovieController : Controller
    {
        private IMovieRepository movieRepository;
        private int pageSize = 4;
        public MovieController(IMovieRepository movieRepository)
        {
            this.movieRepository = movieRepository;
        }
        public IActionResult List(string genre, int page = 1)
        {
            IEnumerable<Movie> cMovie = movieRepository.Movies
            .Where(m => genre == null || m.Genre == genre)
            .OrderBy(m => m.MovieName)
            .Skip((page - 1) * pageSize)
            .Take(pageSize);


            PagingViewModel pagingViewModel = new PagingViewModel()
            {
                CurrentPage = page,
                RecordsPerPage = pageSize,
                TotalRecords = genre == null ?
                movieRepository.Movies.Count() :
                movieRepository.Movies.Where(m => m.Genre == genre).Count()

            };

            MovieListViewModel viewModel = new MovieListViewModel()
            {
                Movies = cMovie,
                PagingViewModel = pagingViewModel,
                CurrentGenre = genre
                
            };

             return View(viewModel);
        }
        //public IActionResult List() => View(movieRepository.Movies);

    }
}
