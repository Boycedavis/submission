﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAB_07.Models;


namespace LAB_07.Controllers
{
    public class OrderController : Controller
    {
        private IOrderRepository orderRepo;
        private PlayList playList;

        public OrderController(IOrderRepository orderRepo, PlayList playList)
        {
            this.orderRepo = orderRepo;
            this.playList = playList;
        }

        public IActionResult Checkout() => View(new Order());

        [HttpPost]
        public IActionResult Checkout(Order order)
        {
            if(playList.Lines.Count() == 0)
            {
                ModelState.AddModelError("", "Sorry, there doesn't seem to be anything in your playList.");
            }
            if (ModelState.IsValid)
            {
                order.Lines = playList.Lines.ToArray();
                orderRepo.SaveOrder(order);
                return RedirectToAction("Complete");
            }
            else
            {
                return View(order);
            }
        }
      

        public IActionResult Complete()
        {
            playList.Clear();
            return View();
        }

        public IActionResult List() => View(orderRepo.Orders.Where(o => !o.Completed));

        [HttpPost]
        public IActionResult MarkCompleted(int orderId)
        {
            Order order = orderRepo.Orders.FirstOrDefault(orderRepo => orderRepo.OrderID == orderId);

            if(order != null)
            {
                order.Completed = true;
                orderRepo.SaveOrder(order);
            }
            return RedirectToAction("List");
        }
    }
}
