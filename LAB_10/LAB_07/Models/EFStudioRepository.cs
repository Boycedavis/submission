﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace LAB_07.Models
{
    public class EFStudioRepository : IStudioRepository
    {
        private Movies2DbContext context;

        public IEnumerable<Studio> Studios => context.Studios;

        public EFStudioRepository(Movies2DbContext context)
        {
            this.context = context;
        }
    }
}
