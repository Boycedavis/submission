﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAB_07.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace LAB_07.Models
{
    public class SessionPlayList : PlayList
    {
        [JsonIgnore]
        public ISession Session {get; set;}

        public static PlayList GetPlayList(IServiceProvider services)
        {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?
                .HttpContext.Session;

            SessionPlayList playList = session?.GetJson<SessionPlayList>("PlayList") ??
            new SessionPlayList();

            playList.Session = session;
            return playList;

        }

        public override void AddMovie(Movie movie)
        {
            base.AddMovie(movie);
            Session.SetJson("PlayList", this);
        }

        public override void RemoveMovie(Movie movie)
        {
            base.RemoveMovie(movie);
            Session.SetJson("PlayList", this);
        }

        public override void Clear()
        {
            base.Clear();
            Session.Remove("PlayList");
        }
    }
}
