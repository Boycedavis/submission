﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_07.Models.ViewModels
{
    public class PlayListIndexViewmodel
    {
        public PlayList PlayList { get; set; }
        public string ReturnUrl { get; set; }
    }
}
