﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_07.Models
{
    public class EFMovieRepository : IMovieRepository
    {
        private Movies2DbContext context;

        public IEnumerable<Movie> Movies => context.Movies;

        public EFMovieRepository(Movies2DbContext context)
        {
            this.context = context;
        }

        public void SaveMovie(Movie movie)
        {
            if (movie.MovieID == 0)
            {
                context.Movies.Add(movie);
                movie.StudioID = 1;
                

            }
            else
            {
                Movie dbMovie = context.Movies.FirstOrDefault(m => m.MovieID == movie.MovieID);
                if(dbMovie != null)
                {
                    dbMovie.MovieName = movie.MovieName;
                    dbMovie.Release = movie.Release;
                    dbMovie.Runtime = movie.Runtime;
                    dbMovie.Genre = movie.Genre;
                }
            }

            context.SaveChanges();
        }
        public Movie DeleteMovie(int movieId)
        {
            Movie efMovie = context.Movies.FirstOrDefault(m => m.MovieID == movieId);
            if (efMovie != null)
            {
                context.Movies.Remove(efMovie);
                context.SaveChanges();
            }
            return efMovie;
        }
    }
}
