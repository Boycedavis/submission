﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_07.Models
{
    public class Movie
    {
        public int MovieID { get; set; }
        [Required(ErrorMessage ="Must enter a title")]
        public string MovieName { get; set; }
        
        [Required(ErrorMessage = "Must enter a genre")]
        public string Genre { get; set; }

        [Required][Range(0.01, int.MaxValue ,ErrorMessage = "Must enter a positive value")]
        public int Runtime { get; set; }

        [Required(ErrorMessage = "Must enter a date")]
        public DateTime? Release { get; set; }

        //Relationships
        public int StudioID { get; set; }
        public Studio Studio { get; set; }

        
        public List<MovieDirector> MovieDirectors { get; set; }
    }
}
