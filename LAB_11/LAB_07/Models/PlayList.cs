﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_07.Models
{
    public class PlayList
    {
        private List<LineItem> movieQueue = new List<LineItem>();

        public virtual IEnumerable<LineItem> Lines => movieQueue;
        public virtual void AddMovie(Movie movie)
        {
            LineItem line = movieQueue
                .Where(m => m.Movie.MovieID == movie.MovieID)
                .FirstOrDefault();
            if (line == null)
            {
                LineItem lineItem = new LineItem()
                {
                    Movie = movie
                };
                movieQueue.Add(lineItem); 
            }
        }

        public virtual void RemoveMovie(Movie movie) => movieQueue.RemoveAll(l=> l.Movie.MovieID == movie.MovieID);
        public virtual int ComputeQueueRuntime()=> movieQueue.Sum(li => li.Movie.Runtime);
        public virtual void Clear() => movieQueue.Clear();


    }
    public class LineItem
     {
         public int LineItemID { get; set; }
         public Movie Movie { get; set; }

     }
     
}
