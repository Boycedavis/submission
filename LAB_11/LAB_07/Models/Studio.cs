﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_07.Models
{
    public class Studio
    {
        public int StudioID { get; set; }
        public string StudioName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        //Associations
        public List<Movie> Movies { get; set; }
    }
}
