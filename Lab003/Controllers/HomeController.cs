﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Lab003.Models;
namespace Lab003.Controllers
{
    public class HomeController : Controller
    {

        //TODO:
        //Flesh out Basic Deliverables

        //Views
        //include _ViewImports x

        //include _BasicLayout x
            //uses ViewBag object for html title x
            //includes @RenderBody() x
        //include __viewStart
            //is mapped using _BasicLayout x
        //include index view
            //strongly-typed model declaration
            //relies on default view, not explicit layout x
            //generates at least one piece of content using a viewbag property x
            //uses razor if-else statement to perform basic action based on model x
            //uses razor for-each statement to display each model object
        //controller
            //uses one action method which creates several of your model objects, places them in an array, and passes the array to the view
        public IActionResult Index()
        {
            Repeater[] repeaterArr = new Repeater[]
            {
                new Repeater()
               { Id = 1, Name = "W9UP", UplinkFreq = 146.970, DownlinkFreq = 146.370, PLTone = 131.8,
                DistanceFromMe = 16.9, CanHear = true, CanKey = true, Operational = false },
                 new Repeater()
               { Id = 2, Name = "W0NE-2", UplinkFreq = 146.835, DownlinkFreq = 146.235, PLTone = 131.8,
                DistanceFromMe = 9, CanHear = true, CanKey = true, Operational = true },
                  new Repeater()
               { Id = 3, Name = "N9TUU", UplinkFreq = 147.000, DownlinkFreq = 147.700, PLTone = 131.8,
                DistanceFromMe = 9.2, CanHear = true, CanKey = true, Operational = false },
                   new Repeater()
               { Id = 4, Name = "N9ETD", UplinkFreq = 147.090, DownlinkFreq = 147.690, PLTone = 131.8,
                DistanceFromMe = 10.9, CanHear = true, CanKey = true, Operational = true },
                    new Repeater()
               { Id = 5, Name = "NOEXE", UplinkFreq = 444.750, DownlinkFreq = 449.750, PLTone = 131.8,
                DistanceFromMe = 16.9, CanHear = false, CanKey = true, Operational = true }

                      


        };


            ViewBag.QTH = "*Distance, Coverage & reliabilty may vary by operating location";
            
            //ViewBag.Operational = Repeater.Operational;
            return View(repeaterArr);
        }
    }
}
