﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab003.Models
{
    public class Repeater
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double UplinkFreq { get; set; }
        public double DownlinkFreq { get; set; }
        public double PLTone { get; set; }
        public double DistanceFromMe { get; set; }
        public bool CanHear { get; set; }
        public bool CanKey { get; set; }
        public bool Operational { get; set; }
    }
}
