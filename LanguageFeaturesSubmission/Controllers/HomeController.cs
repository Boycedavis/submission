﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LanguageFeaturesSubmission.Models;
using Microsoft.AspNetCore.Mvc;

namespace LanguageFeaturesSubmission.Controllers
{
    public class HomeController : Controller
    {
        /*
        public IActionResult Index()
        {
            return View(new string[] { "C#", "Language", "Features" });
        }
        */

        /*
      public IActionResult Index()
      {
          List<string> results = new List<string>();

          foreach (Product p in Product.GetProducts())
          {
              string name = p.Name;
              decimal? price = p.Price;
              results.Add(string.Format("Name: {0}, Price: {1}", name, price));
          }

          return View(results);
      }
      */
        /*
        public IActionResult Index()
        {
            List<string> results = new List<string>();

            foreach (Product p in Product.GetProducts())
            {
                string name = p?.Name;
                decimal? price = p?.Price;
                results.Add(string.Format("Name: {0}, Price: {1}", name, price));
            }

            return View(results);
        }
        */
        /*
          public IActionResult Index()
        {
            List<string> results = new List<string>();

            foreach (Product p in Product.GetProducts())
            {
                string name = p?.Name ?? "<No Name>";
                decimal? price = p?.Price ?? 0 ;
                string related = p?.Related?.Name ?? "<None>";
                results.Add(string.Format("Name: {0}, Price: {1}, Related: {2}", name, price, related));
            }

            return View(results);
        }
         */

        /*
        public IActionResult Index()
        {
            List<string> results = new List<string>();

            foreach (Product p in Product.GetProducts())
            {
                string name = p?.Name ?? "<No Name>";
                decimal? price = p?.Price ?? 0 ;
                string related = p?.Related?.Name ?? "<None>";
                
                results.Add($"Name: {name}, Price: {price}, Related: {related}");
            }

            return View(results);
        }

        private void Test()
        {
            //object initializer
            Product kayak = new Product()
            {
                Name = "Kayak",
                Price = 275M,
                Category = "Water Craft"
            };

            //collection initializer
            string[] names = new string[]
            {
                "Boyce", "Fred", "Joe"
            };

            Dictionary<string, Product> products = new Dictionary<string, Product>()
            {
                { "Kayak", new Product() { Name = "Kayak", Price=275M }},
                {"Lifejacket", new Product() { Name = "Lifejacket", Price=48.95M }} 
            };
          
        }*/
        /*
        public ViewResult Index()
        {
            Dictionary<string, Product> products = new Dictionary<string, Product>()
            {
                { "Kayak", new Product() { Name = "Kayak", Price=275M }},
                {"Lifejacket", new Product() { Name = "Lifejacket", Price=48.95M }}
            };
            return View("Index", products.Keys);
        }
        */
        /*
        public ViewResult Index()
        {
            Dictionary<string, Product> products = new Dictionary<string, Product>()
            {
                 ["Kayak"] = new Product() { Name = "Kayak", Price=275M },
                ["Lifejacket"] = new Product() { Name = "Lifejacket", Price=48.95M }
            };
            return View("Index", products.Keys);
        }
        */
        /*
        public IActionResult Index()
        {
            ShoppingCart cart = new ShoppingCart()
            {
                Products = Product.GetProducts()
            };

            decimal cartTotal = cart.TotalPrices();

            return View("Index", new string[] { $"Total: {cartTotal:C2}" } );
        }
        */
        /*
        public IActionResult Index()
        {
            ShoppingCart cart = new ShoppingCart()
            {
                Products = Product.GetProducts()
            };

            decimal cartTotal = cart.TotalPrices();
            Product[] prodArr =
            {
                new Product(){Name = "Kayak", Price = 275M},
                new Product(){Name = "Lifejacket", Price = 49.95M}



            };

            decimal arrayTotal = prodArr.TotalPrices();

            return View("Index", new string[] {
                $"Total: {cartTotal:C2}",
                $"Array Total: {arrayTotal:C2}"
            });
        }
        */
        /*
        public IActionResult Index()
        {
            Product[] prodArr =
           { 
                new Product(){Name = "Kayak", Price = 275M},
                new Product(){Name = "Lifejacket", Price = 48.95M},
                new Product(){Name = "Soccer Ball", Price = 19.50M},
                new Product(){Name = "Corner Flag", Price = 34.95M}
           };

            decimal arrayTotal = prodArr.FilterByPrice(20).TotalPrices();

            return View("Index", new string[] {
                
                $"Array Total: {arrayTotal:C2}"
            });
        }
        */
        /*
        public IActionResult Index()
        {
            Product[] prodArr =
           {
                new Product(){Name = "Kayak", Price = 275M},
                new Product(){Name = "Lifejacket", Price = 48.95M},
                new Product(){Name = "Soccer Ball", Price = 19.50M},
                new Product(){Name = "Corner Flag", Price = 34.95M}
           };

            decimal arrayTotal = prodArr.FilterByPrice(20).TotalPrices();
            decimal nameFilterTotal = prodArr.FilterByName('S').TotalPrices();

            return View("Index", new string[] {

                $"Array Total: {arrayTotal:C2}",
                $"Name Total: {nameFilterTotal:C2}"
            });
        }
        */
        /*
        bool FilterByPrice(Product p)
        {
            return (p?.Price ?? 0) >= 20;
        }

        bool FilterByName(Product p)
        {
            return p?.Name?[0] == 'S';
        }

        public IActionResult Index()
        {
            Product[] prodArr =
           {
                new Product(){Name = "Kayak", Price = 275M},
                new Product(){Name = "Lifejacket", Price = 48.95M},
                new Product(){Name = "Soccer Ball", Price = 19.50M},
                new Product(){Name = "Corner Flag", Price = 34.95M}
           };

            decimal arrayTotal = prodArr.Filter(FilterByPrice).TotalPrices();
            decimal nameFilterTotal = prodArr.Filter(FilterByName).TotalPrices();

            return View("Index", new string[] {

                $"Array Total: {arrayTotal:C2}",
                $"Name Total: {nameFilterTotal:C2}"
            });
        }
        */
        /*
        public IActionResult Index()
        {
            Product[] prodArr =
           {
                new Product(){Name = "Kayak", Price = 275M},
                new Product(){Name = "Lifejacket", Price = 48.95M},
                new Product(){Name = "Soccer Ball", Price = 19.50M},
                new Product(){Name = "Corner Flag", Price = 34.95M}
           };

            decimal arrayTotal = prodArr
                .Filter(p => (p?.Price ?? 0) >= 20)
                .TotalPrices();
            decimal nameFilterTotal = prodArr
                .Filter(p => p?.Name?[0]  == 'S')
                .TotalPrices();

            return View("Index", new string[] {

                $"Array Total: {arrayTotal:C2}",
                $"Name Total: {nameFilterTotal:C2}"
            });
        }
        */
        /*
        public IActionResult Index()
        {
            IEnumerable<Product> products = Product.GetProducts();
            return View(products.Select(p => p?.Name));
        }
        */
        /*
        public IActionResult Index()
        {
            return View(Product.GetProducts().Where(p => p?.Category == "Water Craft").Select(p => p.Name));
        }
        */

        /*
        public ViewResult Index()
        {
            var names = new[]
            {
                "Kayak", "LifeJacket", "Soccer ball"
            };

            return View(names);
        }
        */

        /*
        public ViewResult Index()
        {
            var products = new[]
            {
                new {Name = "Kayak", Price = 275M},
                new {Name = "Lifejacket", Price = 48.95M},
                new {Name = "Soccer Ball", Price = 19.50M},
                new {Name = "Corner Flag", Price = 34.95M}
            };
            return View(products.Select(p => p?.Name));
        }
        */
        /*
        public ViewResult Index()
        {
            var products = new[]
            {
                new {Name = "Kayak", Price = 275M},
                new {Name = "Lifejacket", Price = 48.95M},
                new {Name = "Soccer Ball", Price = 19.50M},
                new {Name = "Corner Flag", Price = 34.95M}
            };
            return View(products.Select(p => p?.GetType().Name));
        }
        */

        /*
        public async Task<ViewResult> Index()
        {
            long? length = await MyAsyncMethods.GetPageLength();
            return View(new string[] { $"Length: {length}" });
        }
        */
        /*
        public ViewResult Index()
        {
            var products = new[]
            {
                new {Name = "Kayak", Price = 275M},
                new {Name = "Lifejacket", Price = 48.95M},
                new {Name = "Soccer Ball", Price = 19.50M},
                new {Name = "Corner Flag", Price = 34.95M}
            };
            return View(products.Select(p => $"Name: {p?.Name}, Price: {p?.Price}"));
        }
        */

        /*
        public ViewResult Index()
        {
            var products = new[]
            {
                new {Name = "Kayak", Price = 275M},
                new {Name = "Lifejacket", Price = 48.95M},
                new {Name = "Soccer Ball", Price = 19.50M},
                new {Name = "Corner Flag", Price = 34.95M}
            };
            return View(products.Select(p => $"{nameof(p.Name)}: {p?.Name}, {nameof(p.Price)}: {p?.Price}"));
        }
        */
    }
}
