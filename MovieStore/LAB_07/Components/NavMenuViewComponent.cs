﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAB_07.Models;

namespace LAB_07.Components
{
    public class NavMenuViewComponent : ViewComponent
    {

        private IMovieRepository movieRepository;
        public NavMenuViewComponent(IMovieRepository movieRepository)
        {
            this.movieRepository = movieRepository;
        }
        public IViewComponentResult Invoke()
        {
            ViewBag.SelectedGenre = RouteData?.Values["Genre"];
            IEnumerable<string> genres = movieRepository.Movies
                .Select(m => m.Genre)
                .Distinct()
                .OrderBy(g => g);
            return View(genres);
        }
    }
}
