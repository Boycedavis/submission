﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAB_07.Models;

namespace LAB_07.Controllers
{
    public class AdminController : Controller
    {
        private IMovieRepository movieRepo;

        public AdminController(IMovieRepository movieRepo)
        {
            this.movieRepo = movieRepo;
        }

        public IActionResult Index()
        {
            return View(movieRepo.Movies);
        }

        public IActionResult Edit(int movieId)
        {
            return View(movieRepo.Movies.FirstOrDefault(m => m.MovieID == movieId));
        }

        [HttpPost]
        public IActionResult Edit(Movie movie)
        {
            if (ModelState.IsValid)
            {
                movieRepo.SaveMovie(movie);
                TempData["message"] = $"{movie.MovieName} has been saved";
                return RedirectToAction("Index");
            }
            else
            {
                return View(movie);
            }
        }
        public IActionResult Create()
        {
            return View("Edit", new Movie());
        }

        [HttpPost]
        public IActionResult Delete(int movieId)
        {
            Movie delMovie = movieRepo.DeleteMovie(movieId);
            if(delMovie != null)
            {
                TempData["message"] = $"{delMovie.MovieID} was delete";
            }
            return RedirectToAction("Index");
        }
    }
}
