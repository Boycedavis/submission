﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAB_07.Models;
using LAB_07.Models.ViewModels;
using LAB_07.Infrastructure;


namespace LAB_07.Controllers
{
    public class PlayListController : Controller
    {
        private IMovieRepository movieRepository;
        private PlayList playList;

        public PlayListController(IMovieRepository movieRepository, PlayList playListService)
        {
            this.movieRepository = movieRepository;
            playList = playListService;
                 
        }

        public RedirectToActionResult AddToPlayList(int movieId, string returnURL)
        {
            Movie movie = movieRepository.Movies.FirstOrDefault(m => m.MovieID == movieId);
            if(movie != null)
            {
                
                playList.AddMovie(movie);
                
            }
            return RedirectToAction("Index", new { returnURL });
        }

        public RedirectToActionResult RemoveFromPlayList(int movieId, string returnURL)
        {
            Movie movie = movieRepository.Movies.FirstOrDefault(m => m.MovieID == movieId);
            if (movie != null)
            {
               
                playList.RemoveMovie(movie);
               
            }
            return RedirectToAction("Index", new { returnURL });
        } 

        public IActionResult Index(string returnURL)
        {
            PlayListIndexViewmodel model = new PlayListIndexViewmodel()
            {
                PlayList = playList,
                ReturnUrl = returnURL
            };
            return View(model);
        }
        
        
    }
}
