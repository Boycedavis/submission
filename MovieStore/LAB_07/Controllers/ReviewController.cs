﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAB_07.Models;

namespace LAB_07.Controllers
{
    public class ReviewController : Controller
    {

        private IReviewRepository reviewRepo;
        private IMovieRepository movieRepository;
        public ReviewController(IReviewRepository reviewRepo)
        {
            this.reviewRepo = reviewRepo;
        }
       
        /*
         
         todo: 1.add review button to moviedetails page X
               2.make said button navigate to reviewform X
               3.allow reviews to be saved to db with appropriate movieID X
                    3a.
               4.allow reviews for a specific movie to be displayed on that movies detail page 
            
         */


        /*public RedirectToActionResult WriteReview(int movieId, string returnURL)
         {
             Review review = reviewRepo.Movies.FirstOrDefault(m => m.MovieID == movieId);
             if (movie != null)
             {

                 .AddMovie(movie);

             }
             return RedirectToAction("ReviewForm", new { returnURL });


         }*/

        public IActionResult WriteReview(int MovieID)
        {
            Review tmprev = new Review();
            tmprev.MovieID = MovieID;
            return View("ReviewForm", tmprev);
        }

       
        [HttpPost]
        public IActionResult PostReview(Review review)
        {
               

                reviewRepo.SaveReview(review);
            
                return View("Submitted", review);
            
        }

        
    }
}
