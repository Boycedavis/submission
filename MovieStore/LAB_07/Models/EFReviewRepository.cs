﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_07.Models
{
    public class EFReviewRepository : IReviewRepository
    {
        private Movies2DbContext context;

        public IEnumerable<Review> Reviews => context.Reviews;

        public EFReviewRepository(Movies2DbContext context)
        {
            this.context = context;
        }

        public void SaveReview(Review review)
        {

             
                context.Reviews.Add(review);
                
           /* Review dbReview = context.Reviews.FirstOrDefault(r => r.ReviewId == review.ReviewId);
           
                dbReview.MovieID = review.MovieID;
                dbReview.ReviewId = review.ReviewId;
                dbReview.Reviewer = review.Reviewer;
                dbReview.ReviewDesc = review.ReviewDesc;
                dbReview.MovieRating = review.MovieRating;
                
            */



            context.SaveChanges();
        }
    }
}
