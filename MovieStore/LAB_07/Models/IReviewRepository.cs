﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_07.Models
{
    public interface IReviewRepository
    {
        IEnumerable<Review> Reviews { get; }

        void SaveReview(Review review);

    }
}
