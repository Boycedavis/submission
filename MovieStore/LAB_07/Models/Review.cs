﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_07.Models
{
    public class Review
    {
        
        public int ReviewId { get; set; }

        public double MovieRating { get; set; }
        public string Reviewer { get; set; }
        public string ReviewDesc { get; set; }
        public int MovieID { get; set; }
        public Movie Movie { get; set; }


    }
}
