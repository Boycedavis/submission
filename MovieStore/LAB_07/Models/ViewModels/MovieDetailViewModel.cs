﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_07.Models.ViewModels
{
    public class MovieDetailViewModel
    {
        public IEnumerable<Review> Reviews { get; set; }

        public Movie Movie { get; set; }
    }
}
