﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_07.Models.ViewModels
{
    public class ReviewViewModel
    {
        public Review Review { get; set; }
        public string ReturnUrl { get; set; }

    }
}
