using LAB_07.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace LAB_07
{
    /*
     ERD
    ___________________________
   |   MOVIES TABLE            |
   |---------------------------| 
   | MOVIE_ID int(PK)          |______________________________________
   | TITLE string              |                                      |
   | GENRE string              |_                                     |
   | STUDIO_ID int(FK)         |_\                                    |
   | RUNTIME int               |_/|                                   |
   | RELEASE date              |  |                                   |
   |___________________________|  | ____________________________      |
                                  || STUDIOS TABLE              |     |
                                  ||----------------------------|     |   ________________________
                                  L|STUDIO_ID int(PK)           |     |  |                        |
                                   |NAME string                 |     |  |MOVIE DIRECTORS  TABLE  |
                                   |STATE string                |     |  |------------------------|
                                   |CITY string                 |     |  | MD_ID int(PK)          |
                                   |COUNTRY string              |     |__| MOVIE_ID int(FK)       |_
                                   |____________________________|        | DIRECTOR_ID int(FK)    |_\__
                                                                         |________________________|_/ |
                                                                                                      |
                                                                         _________________________    |
                                                                        |DIRCECTORS TABLE         |   |
                                                                        |_________________________|   |
                                                                        |DIRECTOR_ID int(PK)      |___|
                                                                        |FNAME string             |  
                                                                        |LNAME string             |
                                                                        |AGE int                  |
                                                                        |ACTIVE bit               |
                                                                        |_________________________|
     */
    //TODO:
        //Views
            //Use shared layoutX 
            //1 paginated viewX
            //1 partial view
            //Use a viewmodelX
            //Use bootstrapX

        //DB Access
            //Host DB on Azure
            //Use dependency injection
            //Load/Read DB connection string from appsettings.json X

        //Startup
            //Create custom routing for applicationX

        //Genre Navigation
            //get genre view component links to route to appropriatly filtered list view X

        //
    public class Startup
    {
        IConfigurationRoot Configuration;

        public Startup(Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            Configuration = new ConfigurationBuilder().SetBasePath(env.ContentRootPath).AddJsonFile("appsettings.json").Build();
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options => options.EnableEndpointRouting = false);
            
            services.AddDbContext<Movies2DbContext>(options => 
            options.UseSqlServer(Configuration["Data:Movies2:ConnectionString"]));
            
            services.AddTransient<IMovieRepository, EFMovieRepository>();
            services.AddTransient<IStudioRepository, EFStudioRepository>();
            services.AddTransient<IOrderRepository, EFOrderRepository>();
            services.AddTransient<IReviewRepository, EFReviewRepository>();
            services.AddScoped<PlayList>(sp => SessionPlayList.GetPlayList(sp));
         
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddMemoryCache();
            services.AddSession();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSession();
            app.UseRouting();
            app.UseStaticFiles();
            /*app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default", "{controller=Studio}/{action=List}/{id?}");
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default", "{controller=Movie}/{action=List}/{id?}");
            });
            */

            app.UseMvc(routes => {
                
                routes.MapRoute(name: null,
                template: "{genre}/Page{page:int}",
                defaults: new { Controller = "Movie", action = "List" });
                
                routes.MapRoute(name: null,
               template: "Page{page:int}",
               defaults: new { Controller = "Movie", action = "List" , page = 1});

                routes.MapRoute(name: null,
               template: "{genre}",
               defaults: new { Controller = "Movie", action = "List", page = 1 });

                routes.MapRoute(name: null,
               template: "",
               defaults: new { Controller = "Movie", action = "List", page = 1 });

                routes.MapRoute(name: null,
               template: "{controller=}/{action}/{id?}");

               

                routes.MapRoute(name: "studios",
                template: "{controller=Studio}/{action=List}/{id?}");

               
            });
            /*
             
            app.UseMvc(routes => {
                routes.MapRoute(name: "pagination",
                template: "Movies/Page{page}",
                defaults: new { controller = "Movie", action = "List" });
                
                routes.MapRoute(name: "default",
                template: "{controller=Movie}/{action=List}/{id?}");

                routes.MapRoute(name: "studios",
                template: "{controller=Studio}/{action=List}/{id?}");
             */

            //app.UseMvc(routes => { routes.MapRoute(name: "default", template: "{controller=Studio}/{action=List}/{id?}"); });
        }
    }
}
