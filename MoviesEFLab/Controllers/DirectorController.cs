﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoviesEFLab.Models;

namespace MoviesEFLab.Controllers
{
    public class DirectorController : Controller
    {
        private IDirectorRepository directorRepo;

        public DirectorController(IDirectorRepository directorRepo)
        {
            this.directorRepo = directorRepo;
        }

        public IActionResult List()
        {
            return View(directorRepo.Directors.OrderBy(d => d.Lastname)
            .ThenBy(d => d.Firstname));
        }
    }
}
