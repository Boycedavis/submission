﻿using Microsoft.AspNetCore.Mvc;
using MoviesEFLab.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesEFLab.Controllers
{
    public class MovieController : Controller
    {
        private IMovieRepository movieRepo;
        public MovieController(IMovieRepository movieRepo)
        {
            this.movieRepo = movieRepo;
        }
        public IActionResult List() => View(movieRepo.Movies);
       
    }
}
