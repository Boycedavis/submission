﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;

namespace MoviesEFLab.Models
{
    public class EFDirectorRepository : IDirectorRepository
    {
        private MoviesDbContext context;

        public IEnumerable<Director> Directors => context.Directors
            .Include(md => md.MovieDirectors)
            .ThenInclude(m => m.Movie)
            .ThenInclude(s => s.Studio);

            public EFDirectorRepository(MoviesDbContext context)
            {
                this.context = context;
            }
    }
}
