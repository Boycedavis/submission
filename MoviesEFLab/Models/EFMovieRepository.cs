﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MoviesEFLab.Models
{
    public class EFMovieRepository : IMovieRepository
    {
        private MoviesDbContext context;

        public IEnumerable<Movie> Movies => context.Movies;
        
        public EFMovieRepository(MoviesDbContext context)
        {
            this.context = context;
        }

    }
}
