﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesEFLab.Models
{
    public interface IDirectorRepository
    {
        IEnumerable<Director> Directors { get; }
    }
}
