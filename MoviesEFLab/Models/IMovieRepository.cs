﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesEFLab.Models
{
    public interface IMovieRepository
    {
        IEnumerable<Movie> Movies { get; }

    }
}
