﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesEFLab.Models
{
    public class Movie
    {
        public int MovieID { get; set; }
        public string MovieName { get; set; }
        public string Genre { get; set; }
        public int RunTime { get; set; }
        public DateTime? ReleaseDate { get; set; }

        //Associations
        public int StudioID { get; set; }
        public Studio Studio { get; set; }

        public List<MovieDirector> MovieDirectors { get; set; }



    }
}
