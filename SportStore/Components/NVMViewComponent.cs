﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SportsStore.Models;
namespace SportsStore.Components
{
    public class NVMViewComponent : ViewComponent
    {
        private IProductRepository prodRepo;

        public NVMViewComponent(IProductRepository prodRepo)
        {
            this.prodRepo = prodRepo;
        }

        public IViewComponentResult Invoke()
        {

            ViewBag.SelectedCategory = RouteData?.Values["category"];
            IEnumerable<string> categories = prodRepo.Products
                .Select(p => p.Category)
                .Distinct()
                .OrderBy(c => c);
            return View(categories);
        }
    }
}
