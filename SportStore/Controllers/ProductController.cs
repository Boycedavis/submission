﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SportsStore.Models;
using SportsStore.Models.ViewModels;

namespace SportsStore.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository prodRepo;
        private int pageSize = 4;

        public ProductController(IProductRepository prodRepo)
        {
            this.prodRepo = prodRepo;
        }
        /*
        public IActionResult List(int page =1)
        {

            IEnumerable<Product> fProd = prodRepo.Products
                .OrderBy(p => p.Name)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);
            return View(fProd);
        }
        */
        public IActionResult List(string category, int page = 1)
        {

            IEnumerable<Product> fProd = prodRepo.Products
                .Where(p => category == null || p.Category == category)
                .OrderBy(p => p.Name)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            PagingInfo pageInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = pageSize,
                TotalItems = category == null ?
                prodRepo.Products.Count() :
                prodRepo.Products.Where(p=>p.Category == category).Count()
            };

            ProductsListViewModel vModel = new ProductsListViewModel()
            {
                Products = fProd,
                PagingInfo = pageInfo,
                CurrentCategory = category
            };
            return View(vModel);
        }
    }
}
