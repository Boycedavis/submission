﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkingWithVisualStudio.Models;
using System.Threading.Tasks;

namespace WorkingWith_visualStudioUnitTest.Tests
{
    class ProductTests
    {
        [Fact]
        public void CanChangeProductName()
        {
            Product p = new Product { nameof = "Test", Price = 100M};

            p.Name = "New Name";

            Assert.Equal("New Name", p.Name);
        }

        [Fact]
        public void CanChangeProductPrice()
        {
            Product p = new Product { nameof = "Test", Price = 100M };

            p.Price = 200M;

            Assert.Equal(100M, p.Price);

        }
    }
}
