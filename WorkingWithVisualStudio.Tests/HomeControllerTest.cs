﻿using System;
using System.Collections.Generic;
using WorkingWithVisualStudio.Controllers;
using System.Text;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using WorkingWithVisualStudio.Models;
using System.Collections;

namespace WorkingWithVisualStudio.Tests
{
   public class HomeControllerTest
    {
        class ModelCompleteFakeRepository : IRepository
        {
            public IEnumerable<Product> Products { get; } = new Product[]
      {
            new Product() {Name = "P1", Price = 500M},
            new Product() {Name = "P2", Price = 48.95M},
            new Product() {Name = "P3", Price = 19.50M},
            new Product() {Name = "P3", Price = 34.95M}
      };

            public void AddProduct(Product p)
            {
                throw new NotImplementedException();
            }
        }

      

        [Fact]
        public void IndexActionModelIsComplete()
        {
            //Arrange
            HomeController controller = new HomeController();
            controller.Repository = new ModelCompleteFakeRepository();

            //Act
            ViewResult result = (ViewResult)controller.Index();

            IEnumerable<Product> modelProducts = (IEnumerable<Product>)result?.ViewData.Model;

            IEnumerable<Product> repoProducts = SimpleRepository.SharedRepository.Products;


            //Assert
            Assert.Equal(controller.Repository.Products, modelProducts,
                Comparer.Get<Product>((p1, p2) =>
                p1.Name == p2.Name && p1.Price == p2.Price));

        }
    }
}