﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkingWithVisualStudio.Models;
using Xunit;

namespace WorkingWithVisualStudio.Tests
{
     public  class ProductTest
    {
        [Fact]
        public void CanChangeProductName()
        {
            Product p = new Product { Name = "Test", Price = 100M };

            p.Name = "New Name";

            Assert.Equal("New Name", p.Name);

        }

        [Fact]
        public void CanChangeProductPrice()
        {
            Product p = new Product { Name = "Test", Price = 100M };

            p.Price = 200M;

            Assert.Equal(200M, p.Price);
        }
    }
}
