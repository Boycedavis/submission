using System;
using Xunit;
using CD_Tracker.Models;

namespace CD_Tracker.Test
{
    public class CdTest
    {



        [Fact]
        public void WillThrowTermException()
        {
            //Arrange
            CD TestCd = new CD()
            {
                Bank = "",
                Term = 0,
                Rate = 0,
                Start = DateTime.Now,
                Deposit = 0,
                MatureValue = 0


            };
            //Act
            Exception ex = Assert.Throws<Exception>(() => TestCd.Calculate());
            //Assert
            Assert.Equal("Term cannot be less than 1", ex.Message);
        }

        [Fact]
        public void WillThrowRateException()
        {
            //Arrange
            CD TestCd = new CD()
            {
                Bank = "",
                Term = 1,
                Rate = -0,
                Start = DateTime.Now,
                Deposit = 300,
                MatureValue = 0
            };
            //Act
            Exception ex = Assert.Throws<Exception>(() => TestCd.Calculate());
            //Assert
            Assert.Equal("Rate is out of acceptable range", ex.Message);
        }


        [Fact]
        public void WillThrowDepositException()
        {
            //Arrange
            CD TestCd = new CD()
            {
                Bank = "",
                Term = 1,
                Rate = .1,
                Start = DateTime.Now,
                Deposit = 0,
                MatureValue = 0
            };
            //Act
            Exception ex = Assert.Throws<Exception>(() => TestCd.Calculate());
            //Assert
            Assert.Equal("Deposit is too small", ex.Message);
        }


        [Fact]
        public void WillSetMatureValue()
        {
            //Arrange
            CD TestCd = new CD()
            {
                Bank = "",
                Term = 108,
                Rate = .05,
                Start = new DateTime(01 / 21 / 1992),
                Deposit = 800.50,
                MatureValue = 0
            };
            //Act
            TestCd.Calculate();

            //Assert
            Assert.True(TestCd.MatureValue != null);
        }


        [Fact]
        public void WillSetFinishDate()
        {
            //Arrange
            CD TestCd = new CD()
            {
                Bank = "",
                Term = 108,
                Rate = .05,
                Start = new DateTime(01 / 21 / 1992),
                Deposit = 800.50,
                MatureValue = 0
            };
            //Act
            TestCd.Calculate();

            //Assert
            Assert.True(TestCd.Finish != null);
        }
        [Fact]
        public void WillCalculateCorrectMatureValue()
        {
            //Arrange
            CD TestCd = new CD()
            {
                Bank = "",
                Term = 108,
                Rate = .05,
                Start = new DateTime(01 / 21 / 1992),
                Deposit = 800.50,
                MatureValue = 0
            };
            //Act
            TestCd.Calculate();

            //Assert
            Assert.Equal(1255.4, Math.Round(TestCd.MatureValue, 2));
        }
    }
}
